package me.RafaelAulerDeMeloAraujo.Coins;

import me.RafaelAulerDeMeloAraujo.main.Main;




import java.util.UUID;



import java.util.UUID;

public abstract class EconomyHandler {
    protected final Main instance;

    protected EconomyHandler(Main instance) {
        this.instance = instance;
    }

    public abstract boolean hasAccount(UUID uuid);

    public static double get(UUID uuid) {
		// TODO Auto-generated method stub
		return 0;
	}

    public boolean has(UUID uuid, double amount) {
        return get(uuid) >= amount;
    }

    public static boolean set(UUID uuid, double amount) {
		// TODO Auto-generated method stub
		return false;
	}

    public static boolean withdraw(UUID uuid, double amount) {
        return set(uuid, get(uuid) - amount);
    }

    public static boolean deposit(UUID uuid, double amount) {
        return set(uuid, get(uuid) + amount);
    }

    public abstract boolean createAccount(UUID uuid, double startAmount);

    public boolean createAccount(UUID uuid) {
        return createAccount(uuid, 0.0);
    }

    public static boolean deleteAccount(UUID uuid) {
        return false;
    }

    public void disable() {
        // EMPTY
    }
}