package me.RafaelAulerDeMeloAraujo.Coins;



import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.UUID;

import javax.annotation.Nullable;

import lombok.NonNull;
import me.RafaelAulerDeMeloAraujo.main.Main;
import me.lokka30.treasury.api.economy.currency.Currency;
import me.lokka30.treasury.api.economy.response.EconomyException;
import me.lokka30.treasury.api.economy.response.EconomyFailureReason;
import me.lokka30.treasury.api.economy.response.EconomySubscriber;

public class CoinsCurrency implements Currency {
    private final Main instance;

    public CoinsCurrency(Main instance) {
        this.instance = instance;
    }

    @Override
    public @NonNull String getIdentifier() {
        return TreasureEconomyHook.COINS;
    }

    @Override
    public @NonNull String getSymbol() {
        return "";
    }

    @Override
    public char getDecimal() {
        return getActualDecimalPoint();
    }

    @Override
    public @NonNull String getDisplayNameSingular() {
        return "";
    }

    @Override
    public @NonNull String getDisplayNamePlural() {
    	return "";
    }

    @Override
    public int getPrecision() {
        return 1;
    }

    @Override
    public boolean isPrimary() {
        return false;
    }

    @Override
    public void to(@NonNull Currency currency, @NonNull BigDecimal amount, @NonNull EconomySubscriber<BigDecimal> subscription) {
        // There is only one currency here
        subscription.fail(new EconomyException(EconomyFailureReason.FEATURE_NOT_SUPPORTED));
    }

    @Override
    public void parse(@NonNull String formatted, @NonNull EconomySubscriber<BigDecimal> subscription) {
        StringBuilder valueBuilder = new StringBuilder();
        StringBuilder currencyBuilder = new StringBuilder();

        boolean hadDot = false;
        for (char c : formatted.toCharArray()) {
            if (Character.isWhitespace(c)) {
                continue;
            }

            if (!Character.isDigit(c) && !isSeparator(c)) {
                currencyBuilder.append(c);
            } else if (Character.isDigit(c)) {
                valueBuilder.append(c);
            } else if (isSeparator(c)) {
                if (c == getDecimal()) {
                    boolean nowChanged = false;
                    if (!hadDot) {
                        hadDot = true;
                        nowChanged = true;
                    }

                    if (!nowChanged) {
                        valueBuilder = new StringBuilder();
                        break;
                    }
                }
                valueBuilder.append('.');
            }
        }

        if (currencyBuilder.length() == 0) {
            subscription.fail(new EconomyException(FailureReasons.INVALID_CURRENCY));
            return;
        }

        String currency = currencyBuilder.toString();
        if (!matches(currency)) {
            subscription.fail(new EconomyException(FailureReasons.INVALID_CURRENCY));
            return;
        }

        if (valueBuilder.length() == 0) {
            subscription.fail(new EconomyException(FailureReasons.INVALID_VALUE));
            return;
        }

        try {
            double value = Double.parseDouble(valueBuilder.toString());
            if (value < 0) {
                subscription.fail(new EconomyException(EconomyFailureReason.NEGATIVE_BALANCES_NOT_SUPPORTED));
                return;
            }

            subscription.succeed(BigDecimal.valueOf(value));
        } catch (NumberFormatException e) {
            subscription.fail(new EconomyException(FailureReasons.INVALID_VALUE, e));
        }
    }

    private boolean matches(String currency) {
        if (currency.length() == 1) {
            return currency.charAt(0) == getDecimal();
        } else {
            return currency.equalsIgnoreCase(getSymbol())
                    || currency.equalsIgnoreCase(getDisplayNameSingular())
                    || currency.equalsIgnoreCase(getDisplayNamePlural());
        }
    }

    private boolean isSeparator(char c) {
        return c == getDecimal() || c == ',';
    }

    @Override
    public @NonNull BigDecimal getStartingBalance(@Nullable UUID playerID) {
        return BigDecimal.valueOf(0.0);
    }

    @Override
    public @NonNull String format(@NonNull BigDecimal amount, @Nullable Locale locale) {
        return format(amount);
    }

    public @NonNull String format(@NonNull BigDecimal amount, @Nullable Locale locale, int precision) {
        return format(amount, precision);
    }
public String format(BigDecimal amount) {
    return format(amount, getFractionalDigits());
}
public int getFractionalDigits() {
    return 2;
}
public String format(BigDecimal amount, int scale) {
    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
    symbols.setDecimalSeparator(getActualDecimalPoint());
    symbols.setGroupingSeparator(getActualThousandsSeparator());

    DecimalFormat format = new DecimalFormat();
    format.setRoundingMode(RoundingMode.HALF_EVEN);
    format.setGroupingUsed(isUseThousandsSeparator());
    format.setMinimumFractionDigits(0);
    format.setMaximumFractionDigits(scale);
    format.setDecimalFormatSymbols(symbols);
    return format.format(amount);
}
public char getActualDecimalPoint() {
    String point = getDecimalPoint().trim();
    return point.isEmpty() ? '.' : point.charAt(0);
}

public char getActualThousandsSeparator() {
    String separator = getThousandsSeparator().trim();
    return separator.isEmpty() ? ',' : separator.charAt(0);
}
public String getDecimalPoint() {
    return ".";
}
public String getThousandsSeparator() {
    return ",";
}
public boolean isUseThousandsSeparator() {
    return true;
}
}

