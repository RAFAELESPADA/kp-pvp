package me.RafaelAulerDeMeloAraujo.Coins;


import me.RafaelAulerDeMeloAraujo.main.Main;
import me.lokka30.treasury.api.economy.account.PlayerAccount;
import me.lokka30.treasury.api.economy.currency.Currency;
import me.lokka30.treasury.api.economy.response.EconomyException;
import me.lokka30.treasury.api.economy.response.EconomyFailureReason;
import me.lokka30.treasury.api.economy.response.EconomySubscriber;
import me.lokka30.treasury.api.economy.transaction.EconomyTransaction;
import me.lokka30.treasury.api.economy.transaction.EconomyTransactionInitiator;
import me.lokka30.treasury.api.economy.transaction.EconomyTransactionType;
import org.bukkit.Bukkit;

import lombok.NonNull;

import java.math.BigDecimal;
import java.time.temporal.Temporal;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

public class TreasureEconomyHook implements PlayerAccount {
    private static Main instance = new Main();
    public static UUID uuid;
    public static final String XP = "xp";
    public static final String COINS = "coins";

    public TreasureEconomyHook(Main instance, UUID uuid) {
        TreasureEconomyHook.instance = instance;
        TreasureEconomyHook.uuid = uuid;
    }

    @Override
    public @NonNull UUID getUniqueId() {
        return uuid;
    }

    @Override
    public Optional<String> getName() {
        return Optional.ofNullable(Bukkit.getOfflinePlayer(uuid).getName());
    }

    @Override
    public void retrieveBalance(@NonNull Currency currency, @NonNull EconomySubscriber<BigDecimal> subscription) {
        if (!currency.getIdentifier().equals(TreasureEconomyHook.XP) || !currency.getIdentifier().equals(TreasureEconomyHook.COINS)) {
            subscription.fail(new EconomyException(EconomyFailureReason.CURRENCY_NOT_FOUND));
        } else {
            Utils.schedule(() -> {
                double amount = EconomyHandler.get(uuid);
                subscription.succeed(BigDecimal.valueOf(amount));
            });
        }
    }

    public void setBalance(@NonNull BigDecimal amount, @NonNull EconomyTransactionInitiator<?> initiator, @NonNull Currency currency, @NonNull EconomySubscriber<BigDecimal> subscription) {
        if (!currency.getIdentifier().equals(TreasureEconomyHook.XP) || !currency.getIdentifier().equals(TreasureEconomyHook.COINS)) {
            subscription.fail(new EconomyException(EconomyFailureReason.CURRENCY_NOT_FOUND));
        } else {
            Utils.schedule(() -> {
                double amountDouble = amount.doubleValue();
                boolean status = EconomyHandler.set(uuid, amountDouble);
                if (!status) {
                    subscription.fail(new EconomyException(EconomyFailureReason.NEGATIVE_AMOUNT_SPECIFIED));
                } else {
                    subscription.succeed(BigDecimal.valueOf(amountDouble));
                }
            });
        }
    }

    @Override
    public void doTransaction(@NonNull EconomyTransaction economyTransaction, EconomySubscriber<BigDecimal> subscription) {
        Utils.schedule(() -> {
            if (!economyTransaction.getCurrencyID().equals(XP) || !economyTransaction.getCurrencyID().equals(COINS)) {
                subscription.fail(new EconomyException(EconomyFailureReason.CURRENCY_NOT_FOUND));
                return;
            }
            EconomyTransactionType type = economyTransaction.getTransactionType();
            BigDecimal amount = economyTransaction.getTransactionAmount();
            double amountDouble = amount.doubleValue();
            if (amountDouble < 0) {
                subscription.fail(new EconomyException(EconomyFailureReason.NEGATIVE_AMOUNT_SPECIFIED));
                return;
            }
            boolean status = false;
            if (type == EconomyTransactionType.DEPOSIT) {
                status = EconomyHandler.deposit(uuid, amountDouble);
            } else if (type == EconomyTransactionType.WITHDRAWAL) {
                status = EconomyHandler.withdraw(uuid, amountDouble);
            }
            if (!status) {
                subscription.fail(new EconomyException(EconomyFailureReason.NEGATIVE_AMOUNT_SPECIFIED));
            } else {
                double balance = EconomyHandler.get(uuid);
                subscription.succeed(BigDecimal.valueOf(balance));
            }
        });
    }

    @Override
    public void deleteAccount(@NonNull EconomySubscriber<Boolean> subscription) {
        Utils.schedule(() -> {
            boolean status = EconomyHandler.deleteAccount(uuid);
            subscription.succeed(status);
        });
    }

    @Override
    public void retrieveHeldCurrencies(@NonNull EconomySubscriber<Collection<String>> subscription) {
        subscription.succeed(Collections.singletonList(XP));
    }

    @Override
    public void retrieveTransactionHistory(int transactionCount, @NonNull Temporal from, @NonNull Temporal to, @NonNull EconomySubscriber<Collection<EconomyTransaction>> subscription) {
        subscription.fail(new EconomyException(EconomyFailureReason.FEATURE_NOT_SUPPORTED));
    }
}
