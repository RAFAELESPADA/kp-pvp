package me.RafaelAulerDeMeloAraujo.Coins;

import lombok.NonNull;
import me.lokka30.treasury.api.common.response.FailureReason;

public enum FailureReasons implements FailureReason {
    INVALID_VALUE("Invalid value inputted!"),
    INVALID_CURRENCY("Invalid currency inputted!");

    private final String description;

    FailureReasons(String description) {
        this.description = description;
    }

    @Override
    public @NonNull String getDescription() {
        return description;
    }
}