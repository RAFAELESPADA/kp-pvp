package me.RafaelAulerDeMeloAraujo.Coins;


import me.RafaelAulerDeMeloAraujo.main.Main;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.black_ixx.playerpoints.PlayerPoints;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class XP
{
  @SuppressWarnings({ "unchecked", "rawtypes" })
public static HashMap<Player, Integer> bal = new HashMap();
  public static HashMap<Player, Integer> getCoinsMap()
  {
    return bal;
  }
  
  public static int getXP(Player player)
  {
	  Main.ppAPI = PlayerPoints.getInstance().getAPI();
	  if (Bukkit.getPluginManager().isPluginEnabled("PlayerPoints") && Main.ppAPI != null) {
	          
	  return Main.ppAPI.look(player.getUniqueId());
  }
	  else {
	return 0;
  }
  }

  
  public static void setXP(Player player, int amount)
  {
	  Main.ppAPI = PlayerPoints.getInstance().getAPI();
	  if (Bukkit.getPluginManager().isPluginEnabled("PlayerPoints") && Main.ppAPI != null) {
	      
		  Main.ppAPI.set(player.getUniqueId(), amount);
  }
  }
  
  public static void addXP(Player player, int amount)
  {
	  Main.ppAPI = PlayerPoints.getInstance().getAPI();
	  if (Bukkit.getPluginManager().isPluginEnabled("PlayerPoints") && Main.ppAPI != null) {
	      
		  Main.ppAPI.give(player.getUniqueId(), amount);
	  }
  }

  public static void removeXP(Player player, int amount)
  {
	  Main.ppAPI = PlayerPoints.getInstance().getAPI();
	  if (Bukkit.getPluginManager().isPluginEnabled("PlayerPoints") && Main.ppAPI != null) {
	      
		  Main.ppAPI.give(player.getUniqueId(), amount);
	  }
  }
}
 
